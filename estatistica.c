#include <math.h>

double media(int n, double *v){
    double m;
    for(int i = 0; i < n; i++){
        m += v[i];
    }
    return m/n;
}

double desvio(int n, double *v){
	
	double nmedia = media(n, v);
	double soma = 0;

	if(n == 0){
		return 0;
	} else if(n == 1){
		return 0;
	}

	for(int i = 0; i < n; i++){
		soma += pow(v[i] - nmedia, 2);
	}

	double desvio = sqrt(soma/(n-1));	

	return desvio;
}
