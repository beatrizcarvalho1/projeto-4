#define UNITY_INCLUDE_DOUBLE
#include <stdio.h>
#include <math.h>
#include "unity.h"
#include "estatistica.h"

void setUp() {
}

void tearDown() {
}

void test_media_numeros_aleatorios(){
    double vetor[3] = {10, 20, 50};
    double c =  media(3, vetor);
    TEST_ASSERT_DOUBLE_WITHIN(0.001, 26.666, c);
}

void test_media_numeros_negativos(){
    double vetor[4] = {-4, -52, -33, -1};
    double c =  media(4, vetor);
    TEST_ASSERT_DOUBLE_WITHIN(0.01, -22.5, c);
}

void test_media_vetor_vazio(){
    double vetor[] = {};
    double c =  media(0, vetor);
    TEST_ASSERT_DOUBLE_WITHIN(0.001, 0.000, c);
}

void test_media_vetor_um_elemento(){
    double vetor[1] = {-4};
    double c =  media(1, vetor);
    TEST_ASSERT_DOUBLE_WITHIN(0.001, -4, c);
}

void test_media_media_zero(){
    double vetor[2] = {0, 0};
    double c =  media(2, vetor);
    TEST_ASSERT_DOUBLE_WITHIN(0.001, 0.000, c);
}


void test_desvio_numeros_aleatorios(){
    double vetor[3] = {10, 20, 50};
    double c =  desvio(3, vetor);
    TEST_ASSERT_DOUBLE_WITHIN(0.001, 20.816, c);
}

void test_desvio_numeros_negativos(){
    double vetor[4] = {-4, -52, -33, -1};
    double c =  desvio(4, vetor);
    TEST_ASSERT_DOUBLE_WITHIN(0.01, 24.39, c);
}

void test_desvio_vetor_vazio(){
    double vetor[] = {};
    double c =  desvio(0, vetor);
    TEST_ASSERT_DOUBLE_WITHIN(0.001, 0.000, c);
}

void test_desvio_vetor_um_elemento(){
    double vetor[1] = {-4};
    double c =  desvio(1, vetor);
    TEST_ASSERT_DOUBLE_WITHIN(0.001, 0.000, c);
}

void test_desvio_media_zero(){
    double vetor[2] = {0, 0};
    double c =  desvio(2, vetor);
    TEST_ASSERT_DOUBLE_WITHIN(0.001, 0.000, c);
}


int main(){
    double veto[3] = {1, 8, 7};
    UNITY_BEGIN();

    RUN_TEST(test_desvio_numeros_aleatorios);
    RUN_TEST(test_desvio_numeros_negativos);
    RUN_TEST(test_desvio_vetor_vazio);
    RUN_TEST(test_desvio_vetor_um_elemento);
    RUN_TEST(test_desvio_media_zero);

    RUN_TEST(test_media_numeros_aleatorios);
    RUN_TEST(test_media_numeros_negativos);
    RUN_TEST(test_media_vetor_vazio);
    RUN_TEST(test_media_vetor_um_elemento);
    RUN_TEST(test_media_media_zero);
    return UNITY_END();
}
